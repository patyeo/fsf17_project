(function () {
    angular
        .module("ParentConnectApp")
        .controller("AppCtrl", ["$state", "$http", AppCtrl])
        .controller("AddMemberCtrl", ["$state", "$http", AddMemberCtrl])
        .controller("EventCtrl", ["EventServiceAPI", "$uibModal", "$document", "$scope","$state", "$http", EventCtrl])
        .controller("EditEventCtrl", ["$uibModalInstance", "EventServiceAPI", "items", "$rootScope", "$scope", EditEventCtrl])
        .controller("AddEventCtrl", ["uibModalInstance", "EventServiceAPI", "items", "$rootScope", "$scope", AddEventCtrl])
        .controller("DeleteEventCtrl", ["uibModalInstance", "EventServiceAPI", "items", "$rootScope", "$scope", DeleteEventCtrl]);

    function DeleteEventCtrl($uibModalInstance, EventServiceAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteEvent = deleteEvent;
        console.log(items);
        EventServiceAPI.getEvent(items).then((result)=>{
            console.log(result.data);
            self.event =  result.data;
            console.log(self.event.title);
        });

        function deleteEvent(){
            console.log("delete event ...");
            EventServiceAPI.deleteEvent(self.event.evt_id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEventList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddEventCtrl($uibModalInstance, EventServiceAPI, items, $rootScope, $scope){
        console.log("Add Event");
        var self = this;
        self.saveEvent = saveEvent;

//        initializeCalendar($scope);
        function saveEvent(){
            console.log("save event ...");
            console.log(self.event.title);
            console.log(self.event.event_Date);
            EventServiceAPI.addEvent(self.event).then((result)=>{
                //console.log(result);
                console.log("Add event -> " + result.evt_id);
                $rootScope.$broadcast('refreshEventListFromAdd', result.data);
                }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
                })
            $uibModalInstance.close(self.run);
        }
    }
        
/*    function initializeCalendar($scope){
        self.datePattern = /^\d{4}-\d{2}-\d{2}$/;;
        
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
    
        $scope.clear = function() {
            $scope.dt = null;
        };
    
        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
    
        $scope.dateOptions = {
            //dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            console.log(data);
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }
    
        $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };
    
        $scope.toggleMin();
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
    
        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
    
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[1];
        $scope.altInputFormats = ['M!/d!/yyyy'];
    
        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
    
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
        ];
    
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);
        
                for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
        
                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
                }
            }
        
            return '';
        }
    }
*/
    function EditEventCtrl($uibModalInstance, EventServiceAPI, items, $rootScope, $scope){
        console.log("Edit Event Ctrl");
        var self = this;
        self.items = items;
        //initializeCalendar($scope);

        EventServiceAPI.getEvent(items).then((result)=>{
            console.log(result.data);
            self.event =  result.data;
        })

        self.saveEvent = saveEvent;

        function saveEvent(){
            console.log("save event ...");
            console.log(self.event.title);
            console.log(self.event.event_Date);
            EventServiceAPI.updateEvent(self.event).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEventList');
                }).catch((error)=>{
                console.log(error);
                })
            $uibModalInstance.close(self.run);
        }

    }    

function EventCtrl(EventServiceAPI, $uibModal, $document, $scope, $state, $http) {
    var self = this;
    self.format = "M/d/yy h:mm:ss a";
    
    self.events = [];
    self.maxsize=2;
    self.totalItems = 0;
    self.itemsPerPage = 40;
    self.currentPage = 1;

    self.searchEvents =  searchEvents;
    self.addEvent =  addEvent;
    self.editEvent = editEvent;
    self.deleteEvent = deleteEvent;
    self.pageChanged = pageChanged;

    function searchAllEvents(searchKeyword,orderby,itemsPerPage,currentPage){
        EventServiceAPI.searchEvents(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
            self.events = results.data.rows;
            self.totalItems = results.data.count;
            //$scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
        }).catch((error)=>{
            console.log(error);
        });
    }


    function pageChanged(){
        console.log("Page changed " + self.currentPage);
        searchAllEvents(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        console.log($scope.numPages);
    }

    $scope.$on("refreshEventList",function(){
        console.log("refresh event list "+ self.searchKeyword);
        searchAllEvents(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
    });

    $scope.$on("refreshEventListFromAdd",function(event, args){
        console.log("refresh event list from evt_id"+ args.evt_id);
        var events = [];
        events.push(args);
        self.searchKeyword = "";
        self.events = events;
    });

    function searchEvents(){
        console.log("search events  ....");
        console.log(self.orderby);
        searchAllEvents(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
    }

    function addEvent(size, parentSelector){
        console.log("post add event  ....");
        var items = [];
        var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: self.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './app/addEvent.html',
            controller: 'AddEventCtrl',
            controllerAs: 'ctrl',
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return items;
                }
            }
        }).result.catch(function (resp) {
            if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
        });
        
    }

    function editEvent(evt_id, size, parentSelector){
        console.log("Edit Event...");
        console.log("evt_id > " + evt_id);
        
        var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: self.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './app/editEvent.html',
            controller: 'EditEventCtrl',
            controllerAs: 'ctrl',
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return evt_id;
                }
            }
        }).result.catch(function (resp) {
            if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
        });
    }

    function deleteEvent(evt_id, size, parentSelector){
        console.log("delete Event...");
        console.log("evt_id > " + evt_id);
        
        var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: self.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './app/deleteEvent.html',
            controller: 'DeleteEventCtrl',
            controllerAs: 'ctrl',
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return evt_id;
                }
            }
        }).result.catch(function (resp) {
            if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
        });
    }
}



    function AppCtrl($state, $http){
        var AppCtrlself  = this;
        console.log("App controller --> ");
    }

    function AddMemberCtrl($state, $http){
        var AddMemberCtrlself = this;
        console.log("App controller --> ");
        AddMemberCtrlself.onSubmit = onSubmit;
        AddMemberCtrlself.initForm = initForm;
        AddMemberCtrlself.onReset = onReset;
        

        AddMemberCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        AddMemberCtrlself.contactNumberFormat = /^[\s()+-]*([0-9][\s()+-]*){8}$/;
        AddMemberCtrlself.pCodeFormat = /^(\d{6})$/;
        AddMemberCtrlself.user = {
            
        };

/*        AddMemberCtrlself.nationalities = [
            { name: "Please select" , value:0},
            { name: "Singaporean", value: 1},
            { name: "Malaysian", value: 2},
            { name: "Filipino", value: 3},
            { name: "Indonesian", value: 4},
            { name: "Others", value: 5}      
        ];*/

        AddMemberCtrlself.categories = [
            { name: "Please select" , value:0},
            { name: "Parenting - Pre-Schoolers", value: 1},
            { name: "Parenting - Primary-Schoolers", value: 2},
            { name: "Parenting - Teenagers", value: 3},
            { name: "Parenting - Adult Children", value: 4},
            { name: "Marriage", value: 5},
            { name: "Single Parents", value: 6},
            { name: "Spiritual Matters", value: 7}      
        ];

        function initForm(){
            AddMemberCtrlself.user.selectedNationality = "0";
            AddMemberCtrlself.user.gender = "F";
            AddMemberCtrlself.user.selectedCategory = "0";
            //AddMemberCtrlself.user.selectedAge = "0";
            //AddMemberCtrlself.user.selectedNoKid = "0";
        }

        function onReset(){
            AddMemberCtrlself.user = Object.assign({}, regCtrlself.user);
            AddMemberCtrlself.registrationform.$setPristine();
            AddMemberCtrlself.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log("-----start onsubmit-----");
            console.log(AddMemberCtrlself.user.firstName);
            console.log(AddMemberCtrlself.user.email);
            console.log(AddMemberCtrlself.user.password);
            console.log(AddMemberCtrlself.user.confirmpassword);
            console.log(AddMemberCtrlself.user.contactNumber);
            console.log(AddMemberCtrlself.user.gender);
            console.log(AddMemberCtrlself.user.selectedNationality);
            console.log(AddMemberCtrlself.user.pCode);
            console.log(AddMemberCtrlself.user.category);
            $http.post("/api/members", AddMemberCtrlself.user).then((result)=>{
                console.log("result > " + result);
                AddMemberCtrlself.user = result.data;
                console.log("-----after http post-----");
                console.log("firstName > " + AddMemberCtrlself.user.firstName);
                console.log("Email > " + AddMemberCtrlself.user.email);
                console.log("password > " + AddMemberCtrlself.user.password);
                console.log("confirmpassword > " + AddMemberCtrlself.user.confirmpassword);
                console.log("gender > " + AddMemberCtrlself.user.gender);
                console.log("pCode > " + AddMemberCtrlself.user.pCode);
                console.log("selectedNationality > " + AddMemberCtrlself.user.selectedNationality);
                console.log("contactNumber > " + AddMemberCtrlself.user.contactNumber);
            }).catch((error)=>{
                console.log("error > " + error);
            })
        }



        AddMemberCtrlself.initForm();
    }

})();