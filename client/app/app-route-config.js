(function () {
    angular
        .module("ParentConnectApp")
        .config(ParentConnectAppConfig);
        ParentConnectAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function ParentConnectAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('Home',{
                url : '/home',
                templateUrl: "pages/home.html",
                controller : 'AppCtrl',
                controllerAs : 'ctrl'
            })
            .state("SignUp", {
                url: "/signup",
                templateUrl: "pages/signup.html",
                controller : 'AddMemberCtrl',
                controllerAs : 'ctrl'
            })
            .state("SignIn", {
                url: "/signin",
                templateUrl: "pages/signin.html",
                controller : 'SigninCtrl',
                controllerAs : 'ctrl',
                resolve: {
                    user: function(PassportSvc) {
                        return PassportSvc.userAuth()
                        .then(function(result) {
                            return result.data.user;
                        })
                        .catch(function(err) {
                            return '';
                        });
                    }
                    },
                })
            .state("Event", {
                url: "/event",
                templateUrl: "pages/event/event.html",
                controller : 'EventCtrl',
                controllerAs : 'ctrl',
                resolve: {
                    user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                        .then(function(result) {
                        return result.data.user;
                        })
                        .catch(function(err) {
                        return '';
                        });
                    }
                },
                });

        $urlRouterProvider.otherwise("/home");
    
    }

})();