(function(){
    angular
        .module("ParentConnectApp")
        .service("RegServiceAPI", [
            '$http',
            RegServiceAPI
        ])
        .service("EventServiceAPI", [
            '$http',
            EventServiceAPI
        ])
        .service('PassportSvc', ["$http", PassportSvc]);
        
        function PassportSvc($http) {
        var svc = this;
    
        svc.login = login;
        svc.userAuth = userAuth;
    
        function userAuth() {
            return $http.get(
            '/user/auth',
            );
        }
    
        function login(user) {
            return $http.post(
            '/login',
            user
            );
        }
        }
    
    function RegServiceAPI($http){
        var self = this;  
        self.firstName = "";

        self.getfirstName = function(){
            return self.firstName;
        }
        console.log("RegServiceAPI");
        self.register = function(user){
            console.log(user.email);
            console.log(user.password);
            console.log(user.confirmpassword);
            console.log(user.gender);
            console.log(user.firstName);
            console.log(user.lastName);
            console.log(user.pCode);
            console.log(user.Nationality);
            console.log(user.contact);
            self.firstName = user.firstName;
            return $http.post("/api/members", user);
        }
    }

    function EventServiceAPI($http){
        var self = this;
    
        // query string
        self.searchEvents = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/events?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        self.getEvent = function(evt_id){
            console.log(evt_id);
            return $http.get("/api/events/" + evt_id)
        }

        self.updateEvent = function(event){
            console.log(event);
            return $http.put("/api/events",event);
        }

        self.deleteEvent = function(evt_id){
            console.log(evt_id);
            return $http.delete("/api/events/"+ evt_id);
        }
        
        // parameterized values
        /*
        self.searchEmployees = function(value){
            return $http.get("/api/employees/" + value);
        }*/

        // post by body over request
        self.addEvent = function(event){
            return $http.post("/api/events", event);
        }
    }
    

})();