(function() {
    angular
        .module('ParentConnectApp')
        .controller('SigninCtrl', [ 'PassportSvc', '$state' , SigninCtrl]);
    
        function SigninCtrl(PassportSvc, $state) {
        var SigninCtrlself = this;
    
        SigninCtrlself.user = {
            email: "",
            password: ""
        };

        SigninCtrlself.msg = "";
        SigninCtrlself.login = login;

        function login(){
            PassportSvc.login(SigninCtrlself.user)
            .then(function(result) {
            $state.go('home');
            return true;
            })
            .catch(function(err) {
            SigninCtrlself.msg = 'Invalid Username or Password!';
            SigninCtrlself.user.username = SigninCtrlself.user.password = '';
            return false;
            });
        }
        }
    })();