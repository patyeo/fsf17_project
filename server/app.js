"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");
var app = express();
var path = require('path');
var config = require('./development.js');
var session = require ('express-session');
var passport = require ('passport');
var flash = require ('connect-flash');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 3000;
const MEMBER_API = "/api/members";
const EVENT_API = "/api/events";


const SQL_USERNAME = 'root';
const SQL_PASSWORD = 'A!b2c3d4';
var connection = new Sequelize(
    'parentconnect',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);

var Members = require ('./models/members')(connection, Sequelize);
var Events = require ('./models/events')(connection, Sequelize);
var Interest = require ('./models/interests')(connection, Sequelize);
var Member_Interest = require ('./models/memberinterests')(connection, Sequelize);
var Event_Interest = require ('./models/eventinterests')(connection, Sequelize);

/*Member.hasMany(Interest, {foreignKey: 'mbr_id'})
Interest.hasMany(Event, {foreignKey: 'int_id'})

Member_Interest.belongsTo(Member, {foreignKey: 'int_id'})
Event_Interest.belongsToMany(Event, {foreignKey: 'evt_id'})*/

/*Member.findAll().then(member=>{
    console.log(member);
});*/

// create members
app.post( "/api/members", (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    var member = req.body;
    console.log("====server js =====")
    console.log("member firstname :" + member.firstName);
    Members.create(member).then((result)=>{
        console.log(result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
});

// create events
app.post("/api/events", (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    var event = req.body;
    console.log("event title :" + event.title);
    Events.create(event).then((result)=>{
        console.log(result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
});

// update
app.put("/api/events", (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    console.log("update event ..." + req.body);
    console.log(req.body.evt_id);
    var evt_id = req.body.evt_id;
    console.log(evt_id);
    var whereClause = {limit: 1, where: {evt_id: evt_id}};
    Events.findOne(whereClause).then((result)=>{
        result.update(req.body);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

// retrieve 
app.get("/api/events", (req, res)=>{
    console.log("search > " + req.query.keyword);
    var keyword = req.query.keyword;
    var sortby = req.query.sortby;
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
    var offset = (currentPage - 1) * itemsPerPage;
    console.log(offset);
    if(sortby == 'null'){
        console.log("sortby is null");
        sortby = "ASC";
    }
    console.log(keyword);
    console.log(itemsPerPage);
    console.log(currentPage);
    console.log(typeof offset);
    
    console.log("sortby " + sortby);
    console.log(typeof(keyword));
    if(keyword == ''){
        console.log("keyword is empty ?");
    }
    var whereClause = { offset: offset, limit: itemsPerPage, order: [['title', sortby], ['event_Date', sortby]]};
    console.log(keyword.length);
    console.log(keyword !== 'undefined');
    console.log(keyword != undefined);
    console.log(!keyword);
    console.log(keyword.trim().length > 0);

    if((keyword !== 'undefined' || !keyword) && keyword.trim().length > 0){
        console.log("> " + keyword);
        whereClause = { offset: offset, limit: itemsPerPage, order: [['title', sortby], ['event_Date', sortby]], where: {title: keyword}};
    }
    console.log(whereClause);
    Events.findAndCountAll(whereClause).then((results)=>{
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.get("/api/events"+"/:evt_id", (req, res)=>{
    console.log("one event ...");
    console.log(req.params.evt_id);
    var evt_id = req.params.evt_id;
    console.log(evt_id);
    var whereClause = {limit: 1, where: {evt_id: evt_id}};
    Events.findOne(whereClause).then((result)=>{
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.delete("/api/events"+"/:evt_id", (req, res)=>{
    console.log("one event ...");
    console.log(req.params.evt_id);
    var evt_id = req.params.evt_id;
    console.log(evt_id);
    var whereClause = {limit: 1, where: {evt_id: evt_id}};
    Events.findOne(whereClause).then((result)=>{
        result.destroy();
        res.status(200).json({});
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
});













app.use(express.static(__dirname + "/../client/"));


app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app 
