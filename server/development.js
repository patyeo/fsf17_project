'use strict';

//var ENV = process.env.NODE_ENV || 'development';

//module.exports = require(__dirname + '/' + ENV) || {};
module.exports = {
    PORT: process.env.PORT,

    SECRET: process.env.SESSION_SECRET,
    GooglePlus_key: process.env.GOOGLE_PLUS_KEY,
    GooglePlus_secret: process.env.GOOGLE_PLUS_SECRET,
    GooglePlus_callback_url: process.env.GOOGLE_PLUS_CALLBACK_URL,
    Facebook_key: process.env.FACEBOOK_KEY,
    Facebook_secret: process.env.FACEBOOK_SECRET,
    Facebook_callback_url: process.env.FACEBOOK_CALLBACK_URL,

    version: '1.0',
    };