var Sequelize = require ('sequelize');

module.exports = function(connection){
    var Interests = connection.define('events', {
        int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: Sequelize.STRING(500),
            allowNull: false
        },
        category: {
            type: Sequelize.STRING(120),
            allowNull: false
        },
        category_id:{
            type: Sequelize.INTEGER(5),
            allowNull: false
        },
        CreatedAt: {
            type: Sequelize.INTEGER(20),
            allowNull: true
        },
        UpdatedAt: {
            type: Sequelize.INTEGER(20),
            allowNull: true
        },
        isEnabled: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        }
    }, {
        timestamps: false
    });
    return Interests;
}