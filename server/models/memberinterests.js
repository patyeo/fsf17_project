var Sequelize = require ('sequelize');

module.exports = function(connection){
    var MemberInterests = connection.define('memberinterests', {
        mbr_int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        mbr_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            foreignKey: true
        },
        int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            foreignKey: true
        },
        CreatedAt: {
            type: Sequelize.INTEGER(20),
            allowNull: false
        },
        UpdatedAt: {
            type: Sequelize.INTEGER(20),
            allowNull: false
        },
        isEnabled: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        }
    }, {
        timestamps: false
    });
    return MemberInterests;
}