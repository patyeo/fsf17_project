var Sequelize = require ('sequelize');

module.exports = function(connection){
    // return Members object is used within the JS
    var Members = connection.define('members', {
        mbr_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: Sequelize.STRING(25) ,
            allowNull: false
        },
        lastName: {
            type: Sequelize.STRING(25),
            allowNull: false
        },
        email:{
            type: Sequelize.STRING(45),
            allowNull: false
        },
        password: {
            type: Sequelize.STRING(10),
            allowNull: false
        },
        contact: {
            type: Sequelize.INTEGER(15),
            allowNull: true
        },
        pCode: {
            type: Sequelize.INTEGER(6),
            allowNull: false
        },
        gender: {
            type: Sequelize.STRING(1),
            allowNull: true
        },
        nationality: {
            type: Sequelize.STRING(10),
            allowNull: true
        },
        isAdmin: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        },
        joinDate: {
            type: Sequelize.DATE,
            allowNull: true
        },
        resetPasswordDt: {
            type: Sequelize.DATE,
            allowNull: true
        },
        loginCounter: {
            type: Sequelize.INTEGER(3),
            allowNull: true
        },
        resetCounter: {
            type: Sequelize.INTEGER(3),
            allowNull: true
        },
        CreatedAt: {
            type: Sequelize.INTEGER(20),
            allowNull: true
        },
        UpdatedAt: {
            type: Sequelize.INTEGER(20),
            allowNull: true
        },
        isEnabled: {
            type: Sequelize.INTEGER(2),
            allowNull: true
//            defaulValue: "https://cdn.pixabay.com/photo/2014/04/02/10/35/face-303912_960_720.png"
        }

    }, {
        timestamps: false
    });
    return Members;
};
